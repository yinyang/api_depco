from basecore.models import BaseView, ManagerView, MeliManager
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from openpyxl import load_workbook
import mimetypes


def get_column_list(ws):
    column_list = []
    unnamed = 1
    for row in ws.iter_rows():
        for cell in row:
            if cell.value is not None:
                if cell.value in column_list:
                    column_list.append(cell.value + '.' + str(unnamed))
                    unnamed = unnamed + 1
                else:
                    column_list.append(cell.value)
            else:
                column_list.append('Unnamed_' + (
                    '0' + str(unnamed) if unnamed < 10 else str(unnamed)))
                unnamed = unnamed + 1
        break
    return column_list


class ProductView(View, BaseView):
    @staticmethod
    @csrf_exempt
    def upload_products(request, **kwargs):
        manager = ManagerView()
        column_list = []
        if request.method == 'POST':
            meta_file = request.FILES.get('file_front', [])
            wb = load_workbook(filename=meta_file.file)
            for ws in wb.worksheets:
                column_list.append(get_column_list(ws))
        return manager.response(column_list)
