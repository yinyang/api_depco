#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "api_depco.settings")
    try:
        from django.core.management import execute_from_command_line

        # Las siguientes lineas de código permiten depuracion.
        # https://github.com/DonJayamanne/pythonVSCode/issues/637
        if '--ptvsd' in sys.argv:
            sys.argv.remove('--ptvsd')
            import ptvsd

            ptvsd.enable_attach("my_secret", address=('0.0.0.0', 3000))
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    execute_from_command_line(sys.argv)
