#!/bin/bash


DAEMON_PATH="/home/ubuntu/api_depco/backend/api_depco"

DAEMON="sudo -H -u ubuntu bash -c /home/ubuntu/api_depco/services/inicia-todo.sh"


NAME=api_depco-backend
DESC="Servicio api_depco backend"
PIDFILE=/var/run/${NAME}.pid
SCRIPTNAME=/etc/init.d/${NAME}
LOGFILE=/var/log/${NAME}.log

case "$1" in
start)
	printf "%-50s" "Starting ${NAME}..."
	cd /home/ubuntu/api_depco
	sudo -H -u ubuntu bash -c date +"%Y:%m:%d-%H:%M:%S -------------------------------------" >> /var/log/backend-git.log 2>&1
    sudo -H -u ubuntu git checkout -B "master" "origin/master" >> /var/log/backend-git.log 2>&1
    sudo -H -u ubuntu git pull "origin" +refs/heads/master:refs/remotes/origin/master >> /var/log/backend-git.log 2>&1
    sudo -H -u ubuntu bash -c date +"%Y:%m:%d-%H:%M:%S -------------------------------------" >> /var/log/backend-git.log 2>&1
    cd /home/ubuntu/api_depco/stores
    sudo -H -u ubuntu bash -c date +"%Y:%m:%d-%H:%M:%S -------------------------------------" >> /var/log/backend-migrations.log 2>&1
    sudo -H -u ubuntu bash ./migrate-production.sh >> /var/log/backend-migrations.log 2>&1
    sudo -H -u ubuntu /home/ubuntu/Envs/api_depco/bin/python3.6 /home/ubuntu/api_depco/backend/api_depco/manage.py migrate >> /var/log/backend-migrations.log 2>&1
	sudo -H -u ubuntu bash -c date +"%Y:%m:%d-%H:%M:%S -------------------------------------" >> /var/log/backend-migrations.log 2>&1
	PID=`${DAEMON} >> ${LOGFILE} 2>&1 & echo $!`
		if [ -z ${PID} ]; then
			printf "%s\n" "Fail"
		else
			echo ${PID} > ${PIDFILE}
			printf "%s\n" "Ok"
		fi
;;
status)
		printf "%-50s" "Checking $NAME..."
		if [ -f $PIDFILE ]; then
			PID=`cat $PIDFILE`
			if [ -z "`ps axf | grep ${PID} | grep -v grep`" ]; then
				printf "%s\n" "Process dead but pidfile exists"
			else
				echo "Running"
			fi
		else
			printf "%s\n" "Service not running"
		fi
;;
stop)
		printf "%-50s" "Stopping $NAME"
			PID=`cat $PIDFILE`
			cd $DAEMON_PATH
		if [ -f $PIDFILE ]; then
			kill -HUP $PID
			printf "%s\n" "Ok"
			rm -f $PIDFILE
		else
			printf "%s\n" "pidfile not found"
		fi
;;

restart)
	$0 stop
	$0 start
;;

*)
		echo "Usage: $0 {status|start|stop|restart}"
		exit 1
esac
