#!/bin/bash

echo "------------------------------"
echo "CONFIGURANDO SERVICIOS"
echo "------------------------------"

echo "[Unit]
Description=Dataclean backend
After=network.target

[Service]
User=root
Restart=always
Type=forking
ExecStart=/home/ubuntu/api_depco/services/api_depco-backend.sh start
ExecStop=/home/ubuntu/api_depco/services/api_depco-backend.sh stop
ExecReload=/home/ubuntu/api_depco/services/api_depco-backend.sh restart

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/api_depco-backend.service

chmod 755 /etc/systemd/system/api_depco-backend.service

systemctl daemon-reload

systemctl enable api_depco-backend.service
systemctl start api_depco-backend.service
echo "------------------------------"
echo "FINALIZA INSTALACION"
echo "------------------------------"
