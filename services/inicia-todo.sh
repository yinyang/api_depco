#!/bin/bash
cd /home/ubuntu/api_depco/backend/api_depco
sudo -H -u ubuntu /home/ubuntu/Envs/api_depco/bin/python3.6 /home/ubuntu/Envs/api_depco/bin/celery -A api_depco worker -l info --concurrency=10 --max-tasks-per-child=10 -E &
sudo -H -u ubuntu /home/ubuntu/Envs/api_depco/bin/python3.6 /home/ubuntu/Envs/api_depco/bin/celery -A api_depco flower --port=8001 --loglevel=info &
sudo -H -u ubuntu /home/ubuntu/Envs/api_depco/bin/python3.6 /home/ubuntu/api_depco/backend/api_depco/manage.py rungevent 0.0.0.0
