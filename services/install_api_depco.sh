#!/bin/bash
#sudo sed -i -- 's/#deb-src/deb-src/g' /etc/apt/sources.list && sudo sed -i -- 's/# deb-src/deb-src/g' /etc/apt/sources.list
sudo add-apt-repository -y ppa:jonathonf/python-3.6
#sudo curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get -y update
#sudo apt-get -y upgrade
echo "------------------------------"
echo "INSTALANDO PAQUETES NECESARIOS"
echo "------------------------------"
#sudo apt-get -y build-dep build-essential libssl-dev libffi-dev python-dev libevent-dev libcurl4-gnutls-dev libexpat1-dev git-core curl build-essential openssl libssl-dev nodejs git
sudo apt-get -y install build-essential libssl-dev libffi-dev python-dev libevent-dev libcurl4-gnutls-dev libexpat1-dev git-core curl build-essential openssl libssl-dev git virtualbox-guest-additions-iso virtualbox-guest-dkms

sudo chown -R ${USER}:$(id -gn ${USER}) ${HOME}/.config
sudo chown -R ${USER}:$(id -gn ${USER}) ${HOME}/.cache/pip/http
#sudo npm install -g bower
#sudo npm install -g gulp@3.8.11
#sudo npm install -g gulp-imagemin
#sudo npm install -g node-gyp

echo "------------------------------"
echo "INSTALANDO rabbitmq-server"
echo "------------------------------"
sudo apt-get -y install rabbitmq-server
sudo rabbitmq-plugins enable rabbitmq_management

echo "------------------------------"
echo "CONFIGURANDO ENTORNO VIRTUAL"
echo "------------------------------"

sudo apt-get -y build-dep python3.6
sudo apt-get -y install python3.6 python3.6-dev
sudo apt-get -y install python3-pip

python3.6 -m pip install --upgrade pip
pip install -U pip
pip install --upgrade pip


sudo apt-get -y build-dep postgresql postgresql-contrib libmagic-dev python-psycopg2 python-django python-celery python-socketio python-django-cors-headers python-django-extensions python-djangorestframework  python-django-rosetta  python-flower python-sqlalchemy python-pandas python-memcache python-gevent python-magic memcached python-boto3
sudo apt-get -y install postgresql postgresql-contrib libmagic-dev python-psycopg2 python-django python-celery python-socketio python-django-cors-headers python-django-extensions python-djangorestframework  python-django-rosetta  python-flower python-sqlalchemy python-pandas python-memcache python-gevent python-magic memcached python-boto3

sudo python3.6 -m pip install virtualenv
sudo python3.6 -m pip install virtualenvwrapper
echo "export VIRTUALENVWRAPPER_PYTHON='/usr/bin/python3.6'" >> ${HOME}/.bash_profile
echo "export WORKON_HOME='${HOME}/Envs'" >> ${HOME}/.bash_profile
echo "export PROJECT_HOME='${HOME}/Envs'" >> ${HOME}/.bash_profile
echo "export PYTHONPATH='${PROJECT_HOME}/lib/python3.6/site-packages'" >> ${HOME}/.bash_profile
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ${HOME}/.bash_profile
source ${HOME}/.bash_profile
mkproject ${PROJECT_HOME}/api_depco
virtualenv  ${PROJECT_HOME}/api_depco --distribute
source ${PROJECT_HOME}/api_depco/bin/activate


echo "------------------------------"
echo "CONFIGURANDO EL PROYECTO"
echo "------------------------------"
cd ${HOME}
git clone https://gitlab.com/yinyang/api_depco.git
git config --global user.name "Servidor API Depco"
git config --global user.email "api_depcoserver@depcoautopartes.com.mx"

sudo -H -u postgres psql -c "CREATE USER dinosoft PASSWORD 'da8T_mEjUPr8' CREATEDB CREATEROLE;"
sudo -H -u postgres psql -c "CREATE DATABASE dinosoft WITH ENCODING 'UTF8'"

echo "[db_api_depco]
DATABASE_USER: dinosoft
DATABASE_PASSWORD: da8T_mEjUPr8
DATABASE_HOST: 40.76.211.246
DATABASE_PORT: 5432
DATABASE_ENGINE: django.db.backends.postgresql
DATABASE_NAME: dinosoft

[HOST]
ALLOWED_HOSTS : 192.168.200.248 127.0.0.1 testserver

[EMAIL]
EMAIL_HOST : wl1.weblogica.mx
EMAIL_PORT : 465
EMAIL_HOST_USER : adiaz@depcoautopartes.com.mx
EMAIL_HOST_PASSWORD : idontknow
EMAIL_USE_TLS : True
EMAIL_USE_SSL : True

[DNS]
DNS_FRONTEND : 127.0.0.1:4200

KEY: /etc/ssl/api_depco.key
CRT: /etc/ssl/api_depco.crt

[DEBUG]
DEBUG : False" >> ${HOME}/api_depco/api_depco/settings.ini

mkdir ${HOME}/api_depco/logs
touch ${HOME}/api_depco/logs/api_depco.log
mkdir ${HOME}/api_depco/tmp
mkdir ${HOME}/api_depco/tmp/uploads
mkdir ${HOME}/api_depco/tmp/tmp_csv


echo "------------------------------"
echo "INSTALANDO LIBRERIAS PYTHON"
echo "------------------------------"
python3.6 -m pip install Django==1.10.6
python3.6 -m pip install celery==4.0.2
python3.6 -m pip install jira==1.0.10
python3.6 -m pip install python-socketio==1.7.4
python3.6 -m pip install django-cors-headers==2.0.2
python3.6 -m pip install django-extensions==1.7.7
python3.6 -m pip install djangorestframework
python3.6 -m pip install django-rosetta==0.7.12
python3.6 -m pip install django-tus==0.1.0
python3.6 -m pip install rules==1.2.1
python3.6 -m pip install psycopg2==2.7.1
python3.6 -m pip install SQLAlchemy==1.1.9
python3.6 -m pip install pandas==0.20.1
python3.6 -m pip install python-memcached==1.58
python3.6 -m pip install gevent==1.2.1
python3.6 -m pip install gevent-websocket==0.10.1
python3.6 -m pip install flower==0.9.1
python3.6 -m pip install numexpr==2.6.2
python3.6 -m pip install python-magic==0.4.13
python3.6 -m pip install xlrd==1.0.0
python3.6 -m pip install boto3==1.4.6
python3.6 -m pip install openpyxl==2.4.8
python3.6 -m pip install django-sslserver
python3.6 -m pip install pyOpenSSL==17.4.0
python3.6 -m pip install django-storages==1.6.5
#python3.6 -m pip install git+https://github.com/Angelvanoriega/mercadolibre-python.git
#python3.6 -m pip install -r ${HOME}/api_depco/requirements.txt

chmod +x ${HOME}/api_depco/services/inicia-todo.sh
chmod +x ${HOME}/api_depco/services/api_depco-backend.sh
chmod +x ${HOME}/api_depco/services/api_depco-frontend.sh

#sudo ${HOME}/install-backend-service.sh