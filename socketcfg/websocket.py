# from contextlib import closing
import socketio
import logging
from django.core.cache import cache
from django.utils import translation

logger = logging.getLogger('api_depco')


class Notification(socketio.Namespace):
    """
    Class that notifies you when some processing has completed. 

    Prosecutions:
    1.-
    Conversion from one format to another.
    For example: JSON to PostgreSQL.
    --
    2.-
    Validation of formats in the data.
    For example: e-mail that has the appropriate format.
    """

    def on_connect(self, sid, environ):
        """
        We created a room user for each sid. To control messages from other processes.
        In addition, it validates that the user has a session open.
        """
        import string, random
        result = create_room(self, sid, environ)
        try:
            username = self.rooms(sid)[1]
        except IndexError as e:
            logger.info(e)
            username = ''.join(
                random.choice(string.ascii_uppercase + string.digits) for _ in
                range(5))
        set_language(username, environ)
        return result

    def on_disconnect(self, sid):
        """
        """
        delete_room(self, sid)

    def on_file_finished(self, sid, data):
        print("Received in file process")
        RESULT = {'success': True,
                  'message': 'Proceso de importación terminado correctamente.',
                  'data': []}
        self.emit('file_finished', RESULT, room=sid)

    def on_set_language(self, sid, data):
        data['language'] = data['language'].split('-', 1)[0].split(',', 1)[0]
        translation.activate(data['language'])
        username = self.rooms(sid)[1]
        cache.set("%s_language" % username, data['language'])
        #print("set_language:", data, 'El idioma es:',
        #      translation.get_language())

class Message(socketio.Namespace):
    """
    Test
    """

    def on_connect(self, sid, environ):
        print("Connected with sid:" + sid)
        pass

    def on_disconnect(self, sid):
        print("Disconnected")
        pass


def create_room(obj, sid, environ):
    """
    Create a room with name of user.
    """
    from django.contrib.sessions.models import Session
    from django.contrib.auth.models import User

    def get_sessionid():
        if 'HTTP_COOKIE' in environ:
            import re
            try:
                m = re.search('sessionid=(\w+)', environ['HTTP_COOKIE'])
                return m.group(1)
            except AttributeError as identifier:
                return None

    session_id = get_sessionid()
    logger.debug("Connected with sid:" + sid)
    if session_id is None:
        logger.warning('Has not been logged')
        return False
    session = Session.objects.get(session_key=session_id)
    session_data = session.get_decoded()
    uid = session_data.get('_auth_user_id')
    user = User.objects.get(id=uid)
    obj.enter_room(sid=sid, room=user.username)
    logger.info(
        "Room {} of {} created".format(user.username, type(obj).__name__))
    return True


def delete_room(obj, sid):
    """
    Eliminates the room that has the user name.
    """
    # We obtain all the rooms.
    from django.contrib.auth.models import User
    from django.core.exceptions import ObjectDoesNotExist
    rooms = obj.rooms(sid)
    # It is intended that the room is equal to that of the user.
    # We compare all of the rooms.
    for room in rooms:
        try:
            user = User.objects.get(username=room)
            obj.leave_room(sid, user.username)
            logger.info(
                "Room {} of {} deleted".format(room, type(obj).__name__))
            logger.info("{} Disconnected".format(user.username))
            break
        except ObjectDoesNotExist as DoesNotExist:
            pass


def set_language(username, environ):
    """
    Establece el lenguaje cuando se inicia una conección.
    """
    if 'HTTP_ACCEPT_LANGUAGE' in environ:
        language = \
            environ['HTTP_ACCEPT_LANGUAGE'].split('-', 1)[0].split(',', 1)[0]
        cache.set("%s_language" % username, language)
        #logger.debug(cache.get("%s_language" % username))
