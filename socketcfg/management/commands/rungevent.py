import importlib
import inspect
import sys

import socketio
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.core.wsgi import get_wsgi_application
from gevent import pywsgi
from geventwebsocket.handler import WebSocketHandler
from api_depco.settings import get_element_settings

defaults = {
    'SOCKETIO_PORT': 8443,
    'SOCKETIO_POOL_SIZE': 'default',
    'SOCKETIO_ADDR': '127.0.0.1',
    'SOCKETIO_ENABLED': True,
    'SOCKETIO_ASYNC': 'gevent',
    'SOCKETIO_AUTO': True,
    'SOCKETIO_QUEUE': ''
}


class Command(BaseCommand):
    """
    Command built taking as a base: https://github.com/miki725/django-gevent-deploy.

    https://github.com/django/django/core/management/commands/runserver.py
    If you use Rabbit to install the server:
    https://www.rabbitmq.com/install-debian.html
    """

    help = 'Command to start the project Django-mode Gevent and socketio'

    def add_arguments(self, parser):
        """Added arguments."""
        parser.add_argument('addr', nargs='?', type=str, help="IP  adress")
        parser.add_argument('port', nargs='?', type=int, help="Port of address")
        parser.add_argument('pool_size', nargs='?', type=int, help="Pool size")
        parser.add_argument('socketio_enabled', nargs='?', type=bool,
                            help="Convert to socketio application")
        parser.add_argument('socketio_async', nargs='?', type=str,
                            help="async mode server")
        parser.add_argument('socketio_auto', nargs='?', type=bool,
                            help="Automatically detects the type classes namespace that are in the modules websockete of each application.")
        parser.add_argument('socketio_queue', nargs='?', type=str,
                            help="Active message queue, to communicate messages between processes.")

    def handle(self, *args, **options):
        """
        Code to run the server WSGI-based on gevent.

        The DJANGO_SETTINGS_MODULE is not necessary to run, as it runs
        the file in the first wsgi.py from settings.py with the variable:
        WSGI_APPLICATION = 'api_depco.  wsgi.application'
        We get the application wsgi_application from get_wsgi_application.

        Instances of class socketio.Server will automatically use gevent for
        asynchronous operations if the library is installed and eventlet is not installed.
        You can use a view that makes the role of socketio_manage of this library.
        http://gevent-socketio.readthedocs.io/en/latest/_modules/socketio.html#socketio_manage



        source: http://python-socketio.readthedocs.io/en/latest/
        """
        environ = None
        if args:
            raise CommandError('Enter an ip address')

        addr = options['addr'] or getattr(settings, 'SOCKETIO_ADDR',
                                          defaults['SOCKETIO_ADDR'])
        port = options['port'] or getattr(settings, 'SOCKETIO_PORT',
                                          defaults['SOCKETIO_PORT'])
        pool_size = options['pool_size'] or getattr(settings,
                                                    'SOCKETIO_POOL_SIZE',
                                                    defaults[
                                                        'SOCKETIO_POOL_SIZE'])
        socketio_enabled = options['socketio_enabled'] or getattr(settings,
                                                                  'SOCKETIO_ENABLED',
                                                                  defaults[
                                                                      'SOCKETIO_ENABLED'])
        socketio_async = options['socketio_async'] or getattr(settings,
                                                              'SOCKETIO_ASYNC',
                                                              defaults[
                                                                  'SOCKETIO_ASYNC'])
        socketio_auto = options['socketio_auto'] or getattr(settings,
                                                            'SOCKETIO_AUTO',
                                                            defaults[
                                                                'SOCKETIO_AUTO'])
        socketio_queue = options['socketio_queue'] or getattr(settings,
                                                              'SOCKETIO_QUEUE',
                                                              defaults[
                                                                  'SOCKETIO_QUEUE'])

        quit_command = 'CTRL-BREAK' if sys.platform == 'win32' else 'CONTROL-C'

        application = get_wsgi_application()
        if socketio_enabled:
            if socketio_queue:
                from gevent import monkey
                monkey.patch_socket()
                mgr = socketio.KombuManager("{}://".format(socketio_queue))
            else:
                mgr = None
            sio_app = socketio.Server(async_mode=socketio_async,
                                      client_manager=mgr)
            # usamos el middleware socketio
            application = socketio.Middleware(sio_app, application)
            if socketio_auto:
                self.register_namespace(sio_app)
            else:
                environ = {}
                environ['socketio'] = sio_app

        try:
            self.stdout.write(self.style.SUCCESS(
                "Starting development server at https://{}:{}".format(addr,
                                                                      port)))
            # self.stdout.write(self.style.SUCCESS("Quit the server with CONTROL-C."))
            self.stdout.write(self.style.SUCCESS((
                                                     "Django version %(version)s, using settings %(settings)r\n"
                                                     "Starting development server at %(protocol)s://%(addr)s:%(port)s/\n"
                                                     "Quit the server with %(quit_command)s.\n"
                                                 ) % {
                                                     "version": self.get_version(),
                                                     "settings": settings.SETTINGS_MODULE,
                                                     "protocol": "HTTP/WS",
                                                     "addr": '[%s]' % addr,
                                                     "port": port,
                                                     "quit_command": quit_command,
                                                 }))
            # handler_class to activate the transport websocket
            # pywsgi as you have long polling enabled, in case that the browser does not support websocket.
            keyfile = get_element_settings('DNS', 'KEY')
            certfile = get_element_settings('DNS', 'CRT')
            pywsgi.WSGIServer((addr, port), application, spawn=pool_size,
                              handler_class=WebSocketHandler,
                              keyfile=keyfile,
                              certfile=certfile,
                              environ=environ).serve_forever()
        except Exception as e:
            self.stdout.write(e)
            self.stdout.write('')
            sys.exit(0)

    def register_namespace(self, sio_app):
        """
        Records names of space.

        Those applications with Django that start with Django will be discarded. 
        """
        for app in settings.INSTALLED_APPS:
            try:
                lista = app.split('.')
                if lista[0] != 'django':
                    websocket = importlib.import_module(lista[0] + '.websocket')
                    for name, class_ in inspect.getmembers(websocket):
                        if inspect.isclass(class_):
                            sio_app.register_namespace(
                                class_("/{}".format(name.lower())))
                            print("Registered {}".format(name))
            except ImportError:
                pass
