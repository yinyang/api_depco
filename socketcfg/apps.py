from django.apps import AppConfig


class PythonDjangoSocketioConfig(AppConfig):
    name = 'socketcfg'
