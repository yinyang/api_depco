from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from socketcfg.websocket import Notification, Message
from socketcfg import websocket
from .management.commands.rungevent import Command
import socketio
import logging

buffer = []
logger = logging.getLogger('api_depco')

@csrf_exempt
def socketio(request):
    return HttpResponse(True)
