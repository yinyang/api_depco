from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Sessions(models.Model):
    code = models.TextField()
    access_token = models.TextField()
    token_type = models.TextField()
    expires_in = models.TextField()
    scope = models.TextField()
    user_id = models.TextField()
    refresh_token = models.TextField()
    expires_at = models.TextField()
    generated_at = models.DateTimeField(auto_now_add=True)


class SessionManager(models.Manager):
    def create(self, title):
        book = self.create(title=title)
        # do something with the book
        return book


class UserProfile(models.Model):
    from django.contrib.auth.models import User
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    telefono = models.CharField(max_length=15)
    country = models.CharField(max_length=2, blank=True, null=True)
    db_name = models.CharField(max_length=150, editable=False, unique=True)

    class Meta:
        verbose_name = _('UserProfile')
        verbose_name_plural = _('UserProfiles')

    def __str__(self):
        return self.user.username
