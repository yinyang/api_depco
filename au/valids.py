import re
from django.contrib.auth.models import User

from au.strings import AuVar as cons, AuStr as strs, AuRgx as rgx


def valida_registro(username, password, email, first_name, last_name, telefono,
                    country):
    val_username = valida_username(username)
    if val_username[cons.status] is cons.bool_false:
        return val_username
    val_password = valida_password(password)
    if val_password[cons.status] is cons.bool_false:
        return val_password
    val_email = valida_email(email)
    if val_email[cons.status] is cons.bool_false:
        return val_email
    val_first_name = valida_first_name(first_name)
    if val_first_name[cons.status] is cons.bool_false:
        return val_first_name
    val_last_name = valida_last_name(last_name)
    if val_last_name[cons.status] is cons.bool_false:
        return val_last_name
    val_telefono = valida_telefono(telefono)
    if val_telefono[cons.status] is cons.bool_false:
        return val_telefono
    val_country = valida_country(country)
    if val_country[cons.status] is cons.bool_false:
        return val_country
    return response_valids(cons.bool_true)


def valida_password(password):
    password = quit_spaces(password)
    if len(password) <= cons.int_cero:
        return response_valids(cons.bool_false, strs.msg_void_pwd)
    return response_valids(cons.bool_true)


def valida_email(email):
    email = quit_spaces(email)
    if len(email) <= cons.int_cero:
        return response_valids(cons.bool_false, strs.msg_void_email)
    if len(email) > cons.max_len_email:
        return response_valids(cons.bool_false, strs.msg_long_email)
    r = re.compile(rgx.rgx_email)
    if not r.match(email):
        return response_valids(cons.bool_false, strs.msg_invalid_email)
    exist = len(User.objects.filter(email=email))
    if exist > cons.int_cero:
        return response_valids(cons.bool_false, strs.msg_exist_email)
    return response_valids(cons.bool_true)


def valida_first_name(first_name):
    first_name = quit_spaces(first_name)
    if len(first_name) <= cons.int_cero:
        return response_valids(cons.bool_false, strs.msg_void_ftn)
    if len(first_name) > cons.max_len_frtna:
        return response_valids(cons.bool_false, strs.msg_long_first)
    r = re.compile(rgx.rgx_first_name)
    if not r.match(first_name):
        return response_valids(cons.bool_false, strs.msg_invalid_first)
    return response_valids(cons.bool_true)


def valida_last_name(last_name):
    last_name = str(last_name).replace(cons.str_space, cons.str_void)
    if len(last_name) <= cons.int_cero:
        return response_valids(cons.bool_false, strs.msg_void_lst)
    if len(last_name) > cons.max_len_frtna:
        return response_valids(cons.bool_false, strs.msg_long_last)
    r = re.compile(rgx.rgx_first_name)
    if not r.match(last_name):
        return response_valids(cons.bool_false, strs.msg_invalid_lst)
    return response_valids(cons.bool_true)


def valida_telefono(telefono):
    telefono = quit_spaces(telefono)
    if len(telefono) <= cons.int_cero:
        return response_valids(cons.bool_false, strs.msg_void_phone)
    if len(telefono) > cons.max_len_phone:
        return response_valids(cons.bool_false, strs.msg_long_phone)
    r = re.compile(rgx.rgx_telefono)
    if not r.match(telefono):
        return response_valids(cons.bool_false, strs.msg_invalid_phone)
    return response_valids(cons.bool_true)


def valida_country(country):
    country = quit_spaces(country)
    if len(country) < cons.min_len_country:
        return response_valids(cons.bool_false, strs.msg_void_country)
    if len(country) > cons.max_len_country:
        return response_valids(cons.bool_false, strs.msg_long_country)
    import re
    r = re.compile(rgx.rgx_country)
    if not r.match(country):
        return response_valids(cons.bool_false, strs.msg_invalid_country)
    return response_valids(cons.bool_true)


def valida_username(username):
    username = quit_spaces(username)
    if len(username) <= cons.int_cero:
        return response_valids(cons.bool_false, strs.msg_void_username)
    if len(username) > cons.max_len_username:
        return response_valids(cons.bool_false, strs.msg_long_username)
    r = re.compile(rgx.rgx_username)
    if not r.match(username):
        return response_valids(cons.bool_false, strs.msg_invalid_username)
    exist = len(User.objects.filter(username=username))
    if exist > cons.int_cero:
        return response_valids(cons.bool_false, strs.msg_exist_username)
    return response_valids(cons.bool_true)


def response_valids(status=cons.none, message=cons.none):
    return {cons.status: status, cons.message: message}


def quit_spaces(element):
    return str(element).replace(cons.str_space, cons.str_void)
