from django.db import transaction
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from au.models import Sessions
from au.strings import AuVar as cons, AuStr as strs
from au.valids import valida_registro
from basecore.models import BaseView, ManagerView, MeliManager
from django.contrib.auth.models import User
from au.models import UserProfile
from au.action import AuthorizationAction


class AuthView(View, BaseView):
    @staticmethod
    @csrf_exempt
    def get(request, **kwargs):
        manager = ManagerView()
        meli = MeliManager()
        url = meli.get_auth_url()
        data = {'auth_url': url}
        return manager.response(data)

    @staticmethod
    @csrf_exempt
    def exchange_code(request, **kwargs):
        manager = ManagerView()
        try:
            meli = MeliManager()
            code = kwargs['code']
            token = meli.exchange_code(code)
            session = Sessions(
                code=code,
                access_token=token['access_token'],
                token_type=token['token_type'],
                expires_in=token['expires_in'],
                scope=token['scope'],
                user_id=token['user_id'],
                refresh_token=token['refresh_token'],
                expires_at=token['expires_at']
            )
            session.save()
            response = manager.response(['ok'])
        except Exception as error:
            response = manager.response_error(error)
        return response

    @staticmethod
    @csrf_exempt
    def user_information(request):
        manager = ManagerView()
        try:
            meli = MeliManager()
            user_info = meli.user_information()

            response = manager.response(user_info)
        except Exception as error:
            response = manager.response_error(error)
        return response

    @staticmethod
    @csrf_exempt
    def get_items(request):
        manager = ManagerView()
        try:
            meli = MeliManager()
            user_info = meli.get_items()
            response = manager.response(user_info)
        except Exception as error:
            response = manager.response_error(error)
        return response

    @staticmethod
    @csrf_exempt
    @transaction.atomic()
    def register(request):
        if request.method != cons.meth_post:
            return response_client(strs.type_error, strs.msg_post_needed,
                                   cons.http_not_implemented)
        username = get_element_request(request, 'username')
        password = get_element_request(request, 'password')
        email = get_element_request(request, 'email')
        first_name = get_element_request(request, 'first_name')
        last_name = get_element_request(request, 'last_name')
        telefono = get_element_request(request, 'phone')
        country = get_element_request(request, 'country')
        validation = valida_registro(username, password, email, first_name, last_name,
                                     telefono, country)
        if validation[cons.status] is cons.bool_false:
            return response_client(strs.type_error, validation[cons.message])

        User.username = username
        User.password = password
        User.email = email
        User.first_name = first_name
        User.last_name = last_name

        UserProfile.telefono = telefono
        UserProfile.country = country

        action = AuthorizationAction()
        manager = ManagerView()
        try:
            data = action.register(User, UserProfile)
            response = manager.response(data, cons.http_created)
        except Exception as error:
            response = manager.response_error(error)
        return response


def response_client(type_, message, status=None):
    response = JsonResponse({
        'message': message,
        'type': type_
    })
    if type_ == 'ERROR':
        if status is None:
            response.status_code = 400
        else:
            response.status_code = status
    return response


def get_element_request(request, element):
    return str(request.POST.get(element, cons.str_void))
