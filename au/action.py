from au.store import AUUSERALT
from django.contrib.auth.hashers import make_password
from au.strings import get_message


class AuthorizationAction(object):
    @classmethod
    def register(cls, user, userprofile):
        store = AUUSERALT()
        store.aut_userna = user.username
        store.aut_passwo = make_password(user.password)
        store.aut_emailx = user.email
        store.aut_firstn = user.first_name
        store.aut_lastna = user.last_name
        store.aut_phonex = userprofile.telefono
        store.aut_countr = userprofile.country
        register = store.execute()
        register = get_message(register)
        return register
