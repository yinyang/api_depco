from django.utils.translation import ugettext_lazy as _
from basecore.strings import ConsStr, Strings


class AuVar(ConsStr):
    max_len_email = 254
    max_len_frtna = 30
    max_len_phone = 15
    max_len_country = 2
    max_len_username = 60
    min_len_country = max_len_country


class AuStr(Strings):
    msg_void_pwd = _('Enter a password')
    msg_void_email = _('Enter a email')
    msg_void_ftn = _('Enter a first name')
    msg_void_lst = _('Enter a last name')
    msg_void_phone = _('Enter a phone number')
    msg_void_username = _('Enter a username')
    msg_void_country = _('Select a country')
    msg_invalid_first = _('Use a valid first name')
    msg_invalid_lst = _('Use a valid last name')
    msg_invalid_phone = _('Use a valid phone number')
    msg_invalid_country = _('Use a valid country')
    msg_invalid_username = _('Use a valid username')
    msg_invalid_email = _('Use a valid email')
    msg_exist_email = _('Previously registered email')
    msg_exist_username = _('Previously registered username')
    msg_long_email = _('Email too long')
    msg_long_first = _('First name too long')
    msg_long_last = _('Last name too long')
    msg_long_phone = _('Phone number too long')
    msg_long_country = _('Country too long')
    msg_long_username = _('Username too long')
    msg_post_needed = _('Must be accessed with POST method')


class AuRgx:
    rgx_first_name = '^([^\x21-\x26\x28-\x2C\x2E-\x40\x5B-\x60\x7B-\xAC\xAE-\xBF\xF7\xFE]+)$'
    rgx_telefono = '^(([0-9]|[\-])+)$'
    rgx_country = '^(([A-Z]{2})+)$'
    rgx_email = '^([\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}){1,254}$'
    rgx_username = '^[a-z0-9.\-_\S]+$'


def get_message(consulta):
    message_id = consulta['message_id']
    messages = {
        'AUMSG0001': _('User registered correctly'),
    }
    del consulta['message_id']
    consulta['message'] = messages[message_id]
    return consulta