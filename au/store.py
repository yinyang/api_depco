from basecore.models import Store
from basecore.strings import ConsStr as cons


class AUUSERALT(Store):
    aut_userna = cons.str_void
    aut_passwo = cons.str_void
    aut_emailx = cons.str_void
    aut_firstn = cons.str_void
    aut_lastna = cons.str_void
    aut_phonex = cons.str_void
    aut_countr = cons.str_void
