from basecore.models import BaseView, ManagerView
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from melisdk.models import Meli
from django.http import HttpResponse


class UserForgot(View, BaseView):
    @staticmethod
    @csrf_exempt
    def health_check(request):
        meli = Meli(client_id="7178444675112145", client_secret="Wh69qGoZQEvEpdLn74ZDuowVjaMDwu0B",
                    access_token='',
                    refresh_token='')
        url = meli.auth_url(redirect_URI='https://127.0.0.1:8443/redirect')
        return HttpResponse("<a href='" + url + "'>Login</a>")