from django.apps import AppConfig


class MelisdkConfig(AppConfig):
    name = 'melisdk'
