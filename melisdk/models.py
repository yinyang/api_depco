import json
import ssl
from configparser import SafeConfigParser
from urllib.parse import urlencode

import os
import re
import requests
from requests.adapters import HTTPAdapter
from urllib3 import PoolManager
from api_depco.settings import get_element_settings


class Meli(object):
    def __init__(self, client_id, client_secret, access_token=None, refresh_token=None):
        self.client_id = client_id
        self.client_secret = client_secret
        self.access_token = access_token
        self.refresh_token = refresh_token
        self.expires_in = None

        self._requests = requests.Session()
        try:
            self.SSL_VERSION = get_element_settings('meli', 'ssl_version')
            self._requests.mount('https://', SSLAdapter(ssl_version=getattr(ssl, self.SSL_VERSION)))
        except:
            self._requests = requests

        self.API_ROOT_URL = get_element_settings('meli', 'api_root_url')
        self.SDK_VERSION = get_element_settings('meli', 'sdk_version')
        self.AUTH_URL = get_element_settings('meli', 'auth_url')
        self.OAUTH_URL = get_element_settings('meli', 'oauth_url')

    # AUTH METHODS
    def auth_url(self, redirect_URI):
        params = {'client_id': self.client_id, 'response_type': 'code', 'redirect_uri': redirect_URI}
        url = self.AUTH_URL + '/authorization' + '?' + urlencode(params)
        return url

    def authorize(self, code, redirect_URI):
        params = {'grant_type': 'authorization_code', 'client_id': self.client_id, 'client_secret': self.client_secret,
                  'code': code, 'redirect_uri': redirect_URI}
        headers = {'Accept': 'application/json', 'User-Agent': self.SDK_VERSION, 'Content-type': 'application/json'}
        uri = self.make_path(self.OAUTH_URL)

        response = self._requests.post(uri, params=urlencode(params), headers=headers)

        if response.status_code == requests.codes.ok:
            response_info = response.json()
            self.access_token = response_info['access_token']
            if 'refresh_token' in response_info:
                self.refresh_token = response_info['refresh_token']
            else:
                self.refresh_token = ''  # offline_access not set up
                self.expires_in = response_info['expires_in']

            return self.access_token
        else:
            # response code isn't a 200; raise an exception
            response.raise_for_status()

    def get_refresh_token(self):
        if self.refresh_token:
            params = {'grant_type': 'refresh_token', 'client_id': self.client_id, 'client_secret': self.client_secret,
                      'refresh_token': self.refresh_token}
            headers = {'Accept': 'application/json', 'User-Agent': self.SDK_VERSION, 'Content-type': 'application/json'}
            uri = self.make_path(self.OAUTH_URL)

            response = self._requests.post(uri, params=urlencode(params), headers=headers, data=params)

            if response.status_code == requests.codes.ok:
                response_info = response.json()
                self.access_token = response_info['access_token']
                self.refresh_token = response_info['refresh_token']
                self.expires_in = response_info['expires_in']
                return self.access_token
            else:
                # response code isn't a 200; raise an exception
                response.raise_for_status()
        else:
            raise Exception("Offline-Access is not allowed.")

    # REQUEST METHODS
    def get(self, path, params={}):
        headers = {'Accept': 'application/json', 'User-Agent': self.SDK_VERSION, 'Content-type': 'application/json'}
        uri = self.make_path(path)
        print(uri)
        response = self._requests.get(uri, params=urlencode(params), headers=headers)
        return response

    def post(self, path, body=None, params={}):
        headers = {'Accept': 'application/json', 'User-Agent': self.SDK_VERSION, 'Content-type': 'application/json'}
        uri = self.make_path(path)
        if body:
            body = json.dumps(body)

        response = self._requests.post(uri, data=body, params=urlencode(params), headers=headers)
        return response

    def put(self, path, body=None, params={}):
        headers = {'Accept': 'application/json', 'User-Agent': self.SDK_VERSION, 'Content-type': 'application/json'}
        uri = self.make_path(path)
        if body:
            body = json.dumps(body)

        response = self._requests.put(uri, data=body, params=urlencode(params), headers=headers)
        return response

    def delete(self, path, params={}):
        headers = {'Accept': 'application/json', 'User-Agent': self.SDK_VERSION, 'Content-type': 'application/json'}
        uri = self.make_path(path)
        response = self._requests.delete(uri, params=params, headers=headers)
        return response

    def options(self, path, params={}):
        headers = {'Accept': 'application/json', 'User-Agent': self.SDK_VERSION, 'Content-type': 'application/json'}
        uri = self.make_path(path)
        response = self._requests.options(uri, params=urlencode(params), headers=headers)
        return response

    def make_path(self, path, params={}):
        # Making Path and add a leading / if not exist
        if not (re.search("^http", path)):
            if not (re.search("^\/", path)):
                path = "/" + path
            path = self.API_ROOT_URL + path
        if params:
            path = path + "?" + urlencode(params)
        return path


class SSLAdapter(HTTPAdapter):
    """"Transport adapter" that allows us to use SSLv3."""
    def __init__(self, ssl_version=None, **kwargs):
        self.ssl_version = ssl_version
        super(SSLAdapter, self).__init__(**kwargs)

    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = PoolManager(num_pools=connections,
                                       maxsize=maxsize,
                                       block=block,
                                       ssl_version=self.ssl_version)


