from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import RedirectView

from au.views import AuthView
from pr.views import ProductView

admin.autodiscover()
# handler403 = 'permission.views.my_permission_required'
urlpatterns = [
                  url(r'^', include('django.contrib.auth.urls')),
                  url(r'^$', RedirectView.as_view(url='admin/')),
                  url(r'^admin/', include(admin.site.urls)),
                  url(r'^rosetta/', include('rosetta.urls')),
                  url(r'^', include('django.contrib.auth.urls')),
                  url(r'^', include('socketcfg.urls')),
                  # url(r'^accounts/login$', HomeView.login),
                  url(r'^authorization$', AuthView.as_view(),
                      name='authorization'),
                  url(r'^user/information$', AuthView.user_information,
                      name='user_info'),
                  url(r'^products/upload', ProductView.upload_products, name='upload_products'),
                  url(r'^user/items', AuthView.get_items,
                      name='get_items'),
                  url(
                      r'^authorization/exchange_code/(?P<code>(([A-Z]{2}-([a-z0-9]){24}-[0-9]{9})+))$',
                      AuthView.exchange_code,
                      name='exchange_code'),
                  url(r'^register$', AuthView.register, name='register'),
              ] + static(settings.STATIC_URL,
                         document_root=settings.STATIC_ROOT)
