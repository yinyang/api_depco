"""
Módulo para establecer opciones más rápido
Código repetitivo
"""
import logging
import codecs

LOGGER = logging.getLogger('api_depco')
import json
from django.http import HttpResponse, JsonResponse, QueryDict
from zlib import crc32


def set_connect(username,
                password,
                host,
                port,
                database,
                sql='postgresql',
                archivo=False):
    """
    Establecemos motor en SQLAlchemy
    return engine
    """
    from sqlalchemy import create_engine
    try:
        if sql is not 'sqlite':
            url = '%(engine)s://%(username)s:%(password)s@%(host)s:%(port)s/%(database)s' % {
                'engine': sql,
                'username': username,
                'host': host,
                'password': password,
                'port': port,
                'database': database
            }
            engine = create_engine(url)
        else:
            engine = create_engine('sqlite:///{}'.format(archivo))
        if engine:
            LOGGER.info("Conectado a la base de datos %s", database)
    except Exception:
        LOGGER.exception(
            "Error when starting the database engine",
            extra={'ider': 'IMPORTATION-0005'})
    else:
        return engine


def set_value_connect(username, password, alias='default'):
    """
    Método que establece los valores por default de una conexión

    :param alias: Para conectarse a un host y puerto en particular
    :return dict:
    """
    from django.db import connections
    username_ = connections.databases[alias]['USER']
    connect = {
        'username': username_,
        'database': "db_%s" % username,
        'host': connections.databases[alias]['HOST'],
        'port': connections.databases[alias]['PORT'],
        'password': connections.databases[alias]['PASSWORD']
    }
    return connect


# %%
def replace(rep, text):
    """
    Reemplazo múltiple
    """
    import re
    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))
    return pattern.sub(lambda m: rep[re.escape(m.group(0))], text)


def detect_encoding(b):
    bstartswith = b.startswith
    if bstartswith((codecs.BOM_UTF32_BE, codecs.BOM_UTF32_LE)):
        return 'utf-32'
    if bstartswith((codecs.BOM_UTF16_BE, codecs.BOM_UTF16_LE)):
        return 'utf-16'
    if bstartswith(codecs.BOM_UTF8):
        return 'utf-8-sig'

    if len(b) >= 4:
        if not b[0]:
            # 00 00 -- -- - utf-32-be
            # 00 XX -- -- - utf-16-be
            return 'utf-16-be' if b[1] else 'utf-32-be'
        if not b[1]:
            # XX 00 00 00 - utf-32-le
            # XX 00 00 XX - utf-16-le
            # XX 00 XX -- - utf-16-le
            return 'utf-16-le' if b[2] or b[3] else 'utf-32-le'
    elif len(b) == 2:
        if not b[0]:
            # 00 XX - utf-16-be
            return 'utf-16-be'
        if not b[1]:
            # XX 00 - utf-16-le
            return 'utf-16-le'
    # default
    return 'utf-8'


def response(message='', data=None, type_='INFO', errors=None):
    """
    Método de respuesta
    """
    result = {
        'message': message,
        'type': type_
    }
    if data or data == []:
        result['data'] = data
    if errors:
        result['errors'] = errors
    return result


def as_dict(data):
    """
    Convierte un dict data en dict
    Por ejemplo cuando se usa Form.errors.as_data()
    Devuelve algo parecido a esto: 
    {'name': [ValidationError(['Contains characters that are not allowed']), ValidationError(['Error'])]}
    Y lo convierte a esto:
    {'name': ['Contains characters that are not allowed', 'Error']}
    Para hacerlo igual cuando se use Model.clean_fields(), ya que devuelve un ValidationError para cada Field
    """
    dic = {}
    for k, value in data.items():
        lista = []
        for i, value2 in enumerate(value):
            lista.append(list(value2)[0])
        dic[k] = lista
    return dic


def as_method(request, method='POST'):
    """
    Verifica si está entrando como el método permitido
    """
    if request.method != 'GET':
        response = JsonResponse(
            resp(message='Login required as a GET method', type_='ERROR'))
        response.status_code = 405
        return response
    else:
        return True


class Memory:
    def __init__(self, concurrent=None):
        import psutil
        self.poolworker = psutil.Process()
        memory = psutil.virtual_memory()
        self.available = memory.available
        self.total = memory.total
        # // NOTE: Obtener el número de procesos concurrentes
        if concurrent is None:
            self.concurrent = 2
        else:
            self.concurrent = concurrent

    def memory_status(self):
        """
        Obtiene lo que ocupa actualmente el proceso
        """
        # Obtenemos el tamaño del proceso hasta este punto.
        tam = self.poolworker.memory_full_info().uss
        return tam

    def tam_real_register(self, tam_after, tam_before, count=1000):
        tam_real_register = (tam_after - tam_before) / count
        return tam_real_register

    def available_memory_process(self, tam_after, tam_before, reduction=5,
                                 percent=0.8):
        """
        Permite deteminar disponibilidad a partir de un número
        Fuente (ejemplo): www.postgresql-archive.org/maximum-size-limit-for-a-query-string-td4461395.html
        """
        # Calculamos la memoria necesaria para ser obtimos en el proceso
        available = (self.available / self.concurrent) * percent
        # Restamos la memoria disponible para darle procesamiento a proceso ajeno
        available = available - ((tam_after - tam_before) * reduction)
        return available


# %%
def checksum(buffr=None, file_path=None, buffersize=65536, crcvalue=0,
             format_hex=False):
    """

    Método que calcula el checksum CRC de 32 bits
    Requiere de un buffer o una dirección de archivo
    Solo calcula uno a la vez (buffer o file_path)
    """
    if file_path:
        with open(file_path, 'rb') as afile:
            buffr = afile.read(buffersize)
            while len(buffr) > 0:
                crcvalue = crc32(buffr, crcvalue)
                buffr = afile.read(buffersize)
    elif buffr:
        crcvalue = crc32(buffr, crcvalue)
    if format_hex:
        return format(crcvalue & 0xFFFFFFFF, '08X')
    else:
        return crcvalue


def query_data(request):
    """
    Úsese para método post y put
    Retorna un queryset, sin importar si es un json o un form-data
    """
    if request.content_type == 'multipart/form-data' \
            or request.content_type == 'application/x-www-form-urlencoded':
        return request.POST
    elif request.content_type == 'application/json':
        querydict = QueryDict(mutable=True)
        tojson = request.body
        tojson = tojson.decode(detect_encoding(tojson), 'surrogatepass')
        query = json.loads(tojson)
        querydict.update(query)
        return querydict
        # checksum(file_path = '/vagrant/archivos/a8f4996d3be3436fa0ad3157aa2c4be7_gamonth.json')
