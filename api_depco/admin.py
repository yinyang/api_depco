from django.contrib.admin import AdminSite
from django.contrib import admin
from django.utils.translation import ugettext_lazy


class MyAdminSite(AdminSite):
    '''Creamos un sitio de administrador personalizado.'''

    site_header = ugettext_lazy('Administration of the backend')
    site_title = ugettext_lazy('Site management')
    index_title = ugettext_lazy('Administrative applications')
    admin.autodiscover()


ADMIN_SITE = MyAdminSite(name='myadmin')
