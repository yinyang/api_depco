"""
Validators:
https://docs.djangoproject.com/en/1.10/ref/validators/
https://docs.djangoproject.com/en/dev/ref/models/instances/?from=olddocs#django.db.models.Model.full_clean
"""
#%%
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from django.core.validators import RegexValidator

# def character_not_allowed_(value):
#     """
#     Este método será cambiado en el futuro
#     https://docs.python.org/3/library/stdtypes.html#truth-value-testing
#     """
#     resultado = re.search(r"(^\s+$)|[^a-zA-Z0-9\s_-]+", value)
#     if resultado is not None:
#         raise ValidationError(_('Contains characters that are not allowed'))

CHARACTER_NOT_ALLOWED = RegexValidator(
    regex=r"(^\s+$)|[^a-zA-Z0-9\s_-]+",
    inverse_match=True,
    #regex= r"[^\s\W]+[a-zA-Z0-9\s_-]+[^\s\W]+",
    # Translators: No permite caracteres extraños como #&%$/()
    message=_('Contains characters that are not allowed'))


def error(value):
    raise ValidationError("Error", code='algo')
# from permission.models import Project
# from django.contrib.auth.models import User
# user = User.objects.get(username='fojeda')
# project = Project(name="prueba", user=user)
# project.
# project.save()