"""
Se basó de la siguiente fuente
https://docs.djangoproject.com/en/1.11/_modules/django/utils/log/
"""
import logging
from copy import copy
from contextlib import closing
from jira import JIRA
from django.conf import settings
from django.core.cache import cache
from django.views.debug import ExceptionReporter
from django.core.management.color import color_style
from django.utils.translation import override as ov
from django.utils import translation
import socketio
from kombu import Exchange, Queue


class JiraHandler(logging.Handler):
    """
    Clase que utiliza la librería jira de python.

    Automatiza las incidencias de JIRA.
    Está desarrollado para ser utilizado a través de los logs.
    Cuando se crea un log de tipo Exception o Error.
    Información: https://github.com/pycontribs/jira
    Se requiere habilitar el Remote API
    https://confluence.atlassian.com/doc/enabling-the-remote-api-150460.html
    """

    def __init__(self,
                 project=None,
                 username=None,
                 password=None,
                 component='Backend',
                 include_html=False):
        """
        Inicia atributos.
        Ejemplos: https://jira.readthedocs.io/en/master/examples.html
        """
        logging.Handler.__init__(self)
        options = {
            'server': 'https://jira.atlassian.com'

        }
        # self.jira = JIRA(options, basic_auth=(username, password))
        self.jira = JIRA(options)
        self.component = component
        self.username = username
        self.project = project
        self.include_html = include_html

    def emit(self, record):
        """
        Proceso que se hace a través de logs.
        """
        # request = record.request
        # Obtenemos información
        try:
            request = record.request
            summary = '%s (%s IP): %s' % (record.levelname, (
                'internal'
                if request.META.get('REMOTE_ADDR') in settings.INTERNAL_IPS
                else 'EXTERNAL'), record.getMessage())
        except Exception:
            summary = '%s: %s' % (
                record.levelname,
                record.getMessage()
            )
            request = None

        # Copiamos el registro para no usar la excepción
        no_exc_record = copy(record)
        no_exc_record.exc_info = None
        no_exc_record.exc_text = None

        if record.exc_info:
            exc_info = record.exc_info
        else:
            exc_info = (None, record.getMessage(), None)

        reporter = ExceptionReporter(request, is_email=True, *exc_info)
        message = "%s\n\n%s" % (self.format(no_exc_record),
                                reporter.get_traceback_text())
        html_message = reporter.get_traceback_html() if self.include_html else None
        # Obtener el id en el entorno
        # environment = self.get_environment(no_exc_record)
        environment = getattr(record, 'ider', None)
        descripcion = "%s\n\n%s" % (summary, record.exc_text)
        # print(self.username)
        # print(message)
        print(descripcion)
        print(self.jira.projects())
        return

        # Buscar la incidencia reportada por el admin, con el projecto actual y entorno.
        issue = self.jira.search_issues(
            'assignee={} AND project={} AND component={} AND environment={}'.format(
                self.username, self.project, self.component, environment))
        if issue:
            # Si la incidencia ha sido resuelta.
            if issue.fields.status == 'Resolved' and issue.fields.status == 'Closed':
                # Reabrimos
                issue.update(status='Reopened')
                issue.delete()
                if html_message:
                    comment = self.jira.add_comment(issue, html_message)
                else:
                    comment = self.jira.add_comment(issue, message)
                comment.delete()
        else:
            # Creamos una incidencia
            new_issue = self.jira.create_issue(
                project=self.project,
                summary=summary,
                descripcion=descripcion,
                issuetype={'name': 'Error'},
                environment=environment)
            # Guardamos
            new_issue.delete()
            if html_message:
                comment = self.jira.add_comment(new_issue, html_message)
            else:
                comment = self.jira.add_comment(issue, message)
            comment.delete()

    def format_summary(self, summary):
        """
        Escapar CR y LF caracteres.
        """
        return summary.replace('\n', '\\n').replace('\r', '\\r')

    def get_environment(self, no_exc_record):
        """
        Obtiene el id del error.
        """
        import re
        search = re.search(r'(ID:(\d+))', no_exc_record.getMessage(), re.I)
        if search:
            return search.group(1)
        else:
            return 'El programador no registró un ID'


class RequireEnvTrue(logging.Filter):
    """
    """

    def filter(self, record):
        return settings.ENV


class ServerFormatter(logging.Formatter):
    """
    Formateador
    """

    def __init__(self, *args, **kwargs):
        self.style = color_style()
        super(ServerFormatter, self).__init__(*args, **kwargs)

    def format(self, record):
        msg = record.msg
        status_code = getattr(record, 'status_code', None)

        if status_code:
            if 200 <= status_code < 300:
                # Put 2XX first, since it should be the common case
                msg = self.style.HTTP_SUCCESS(msg)
            elif 100 <= status_code < 200:
                msg = self.style.HTTP_INFO(msg)
            elif status_code == 304:
                msg = self.style.HTTP_NOT_MODIFIED(msg)
            elif 300 <= status_code < 400:
                msg = self.style.HTTP_REDIRECT(msg)
            elif status_code == 404:
                msg = self.style.HTTP_NOT_FOUND(msg)
            elif 400 <= status_code < 500:
                msg = self.style.HTTP_BAD_REQUEST(msg)
            else:
                # Any 5XX, or any other status code
                msg = self.style.HTTP_SERVER_ERROR(msg)

        if self.uses_server_time() and not hasattr(record, 'server_time'):
            record.server_time = self.formatTime(record, self.datefmt)

        record.msg = msg
        return super(ServerFormatter, self).format(record)

    def uses_server_time(self):
        """
        Obtiene el tiempo del servidor.
        """
        return self._fmt.find('%(server_time)') >= 0


class NotificationHandler(logging.Handler):
    """
    Clase que notifica al usuario a través de WebSocket
    Utiliza la librería socketio
    Es obligatorio como atributo adicional el socket
    Ejemplo de uso:
        import logging
        from django.utils.translation import override
        logger = logging.getLogger('api_depco')
        try:
            raise ValueError("Error de prueba :)")
        except ValueError as identifier:
            logger.error(identifier, extra={'socket':'file_finished'})
    Con este ejemplo emitirá un mensaje al cliente via websocket.
    Usando el mensaje de error, al evento file_finished.
    """

    def __init__(self, namespace):
        logging.Handler.__init__(self)
        self.namespace = namespace

    def emit(self, record):
        """
        Emite un mensaje
        """
        external_sio = socketio.KombuManager('amqp://', write_only=True)
        language = cache.get("%s_language" % getattr(record, 'room', None))
        translation.activate(language)
        result = {
            'message': record.getMessage(),
            'data': getattr(record, 'data', []),
            'type': getattr(record, 'type', record.levelname)
        }
        data = getattr(record, 'data', [])
        if data:
            result['data'] = data
        errors = getattr(record, 'errors', [])
        if errors:
            result['errors'] = errors
        status = getattr(record, 'status', None)
        if status:
            result['status'] = status
        if result['message'] == '':
            del result['message']
        if result['type'] == '':
            del result['type']
        external_sio.emit(
            record.socket,
            result,
            namespace='/%s' % (getattr(record, 'namespace', self.namespace)),
            room=getattr(record, 'room', None))


class RequireSocketTrue(logging.Filter):
    """
    Valida si se enviará el log al usuario a través de sockets.
    """

    def filter(self, record):
        return getattr(record, 'socket', False)


class RequiereLogTrue(logging.Filter):
    """
    Filtro que se usa para saber si se guarda el log en los archivos logs
    """

    def filter(self, record):
        if not record.msg:
            return False
        return True


class FileFormatter(logging.Formatter):
    """
    Para traducir el idioma en ingles
    """

    def __init__(self, *args, **kwargs):
        self.style = '%'
        super(FileFormatter, self).__init__(*args, **kwargs)

    def format(self, record):
        msg = record.msg
        with ov('en'):
            record.msg = msg
        return super(FileFormatter, self).format(record)
