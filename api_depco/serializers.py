"""
Serializadores del proyecto
"""
import json
class DictJsonEncoder(json.JSONEncoder):
    """
    Método que convierte un dict-json en json
    """
    def default(self, o):
        if isinstance(o, dict):
            #Es un str
            data = o.get('data')
            #Eliminamos el key data con todo su contenido
            del o['data']
            print(o)
            #La creamos de nuevo
            o.update({'data':'{%%}'})
            #Convertimos el o en json
            json_str = json.dumps(o)
            #Realizamos un replace
            new_json = json_str.replace('{%%}', data)
            return new_json
        return json.JSONEncoder.default(self, o)
